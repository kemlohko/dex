# README #

* This is one of my most complex blockchain projects. I implemented a Dezentralized Exchange on Ethereum.
* I have two smart contract, one for my own ERC20 token and the other for the exchange.
* The Frontend was implemented with React and i use web3 to communication with the smart contracts.
* At the End the project will be hosted on heroku and the smart contracts will be deployed on the Ethereum main network.


### How do I get set up? ###

* I use ganache for testing purposes.
* Run the tests with 'truffle test'.
* Run the seed-exchange script with 'truffle exec [scripts]'.
* Deploy the smart contracts with 'truffle migrate'.
* Start the server with 'npm start'.

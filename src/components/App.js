import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import Web3 from 'web3';
import Token from '../abis/Token.json'
import { loadWeb3, loadAccount, loadToken, loadExchange } from '../store/interactions';
import { contractLoadedSelector } from '../store/selectors';
import Navbar from './Navbar';
import Content from './Content';

class App extends Component {
  componentDidMount() {
    this.loadBlockchainData(this.props.dispatch)

  }

  async loadBlockchainData(dispatch) {
    const web3 = loadWeb3(dispatch)
    const network = await web3.eth.net.getNetworkType()
    const networkId = await web3.eth.net.getId()
    await loadAccount(web3, dispatch)
    const token = await loadToken(web3, networkId, dispatch)
    if (!token) {
      window.alert('Token smart contract not detected on the current network. Please select another network with Metamask.')
    }
    const exchange = loadExchange(web3, networkId, dispatch)
    if (!exchange) {
      window.alert('Exchange smart contract not detected on the current network. Please select another network with Metamask.')
    }
 
  }


  render() {
   
    return (
      <div>
        <Navbar />
        { this.props.contractsLoaded ? <Content /> : <div className= "content" /> }
      </div>
    );
  }

}

// map the state to props
function mapStateToProps(state) {
  return {
    'contractsLoaded': contractLoadedSelector(state)
  }
}


export default connect(mapStateToProps)(App);

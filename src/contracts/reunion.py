class Member:
    
    def __init__(self, name, fondsDeCaisse, pourcentageFondsDeCaisse, dettes, sanctions, benefice ):
        super().__init__()
        self.name = name
        self.fondsDeCaisse = fondsDeCaisse
        self.pourcentageFondsDeCaisse = pourcentageFondsDeCaisse
        self.dettes = dettes
        self.sanctions = sanctions
        self.benefice = benefice

def calculTotalSanctions(members):
    sanction = 5 + 10.64 - 35
    for member in members:
        sanction = sanction + member.sanctions
    return sanction


def calculBeneficeDeChaqueMembre(members, fondsDeCaisseTotal):
    total = 0
    amande = (calculTotalSanctions(members) + 205) / 14
    ben = 0
    dettes = 0
    for member in members:
        member.benefice = (fondsDeCaisseTotal * member.pourcentageFondsDeCaisse / 100)
        print('membre:', member.name, 'benefice:', member.benefice)
        total = total + member.benefice
        
        if member.dettes > 0:
            member.benefice = member.benefice - member.dettes
        print('benefice apres deduction de l emprunt:', member.benefice)

        if member.sanctions > 0:
            member.benefice = member.benefice - member.sanctions
        print('benefice apres deduction des sanctions', member.benefice)
        print('benefice apres partage des amandes et fonds de caisse extra:', member.benefice + amande)
        print('**********************************************************************')
        ben = ben + member.benefice
        dettes = dettes + member.dettes

    print('total:', total)
    print('ben: ', ben)
    print('dettes: ', dettes)
    return members


m1 = Member('Kemloh Alex', 250, 19.91, 0, 5, 0)
m2 = Member('Ngoufack Silvere Sacker', 200, 15.93, 0, 7+5, 0)
m3 = Member('Nkounga Wabinwa Joel', 200, 15.93, 0, 8, 0)
m4 = Member('Kamga Nono Alex', 100, 7.97, 0, 0, 0)
m5 = Member('Tonfack Ramsay', 100, 7.97, 0, 5+20, 0)
m6 = Member('Djiedzole Joel Blondel', 100, 7.97, 97.29+105, 25, 0)
m7 = Member('Kamgang Franck Arthur', 55.42, 4.41, 115, 8, 0)
m8 = Member('Djiedzole Penka Ulrich', 50, 3.98, 157.75, 0, 0)
#m7 = Member('Kamgang Franck Arthur', 0, 0, 0, 0, 0)
#m8 = Member('Djiedzole Penka Ulrich', 0, 0, 0, 0, 0)
m9 = Member('Kepdjio Wakam Champion', 50, 3.98, 0, 5, 0)
m10 = Member('Nkouamedjo Rudel Christian', 30, 2.39, 0, 0, 0)
m11 = Member('Ghanou Dali Reneau', 30, 2.39, 0, 21+25, 0)
m12 = Member('Deufi Franck', 30, 2.39, 0, 0, 0)
m13 = Member('Kenne Bidias', 30, 2.39, 131.25, 15, 0)
m14 = Member('Tekoulegha Rossignol', 30, 2.39, 0, 0, 0)
members = [m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14]
calculBeneficeDeChaqueMembre(members, 1428.51)

print('total sanctions: ', calculTotalSanctions(members))



